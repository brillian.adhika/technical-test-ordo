<div class="sidebar p-3">
    <div class="welcome d-flex align-items-center">
        <img src="{{ asset('assets/img/logo-square 1.png') }}" alt="">
        <div class="text">
            <span>Halo, Selamat Datang</span>
            <h3>John Doe</h3>
        </div>
    </div>

    <div class="image-area d-flex align-items-center mb-4">
        <div class="image">
            <img src="{{ asset('assets/img/avatar.png') }}" alt="">
            <div class="online-status online"></div>
        </div>
        <div class="text">
            <h3>John Doe</h3>
            <span>Kelas 7-A | No. Absen 17</span>
        </div>
    </div>

    <ul class="nav nav-pills flex-column mb-auto">
        <li class="nav-item">
            <a href="{{ url('/') }}" class="nav-link {{ Request::path() == '/' ? 'active' : '' }}" aria-current="page">
                <img src="{{ Request::path() == '/' ? 'assets/img/dashboard-active.png' : 'assets/img/dashboard.png' }}"
                    alt="">
                <span>Dashboard</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ url('profile') }}" class="nav-link {{ Request::path() == 'profile' ? 'active' : '' }}" aria-current="page">
                <img src="{{ Request::path() == 'profile' ? 'assets/img/profile-active.png' : 'assets/img/profile.png' }}"
                    alt="">
                <span>Profile</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link" aria-current="page">
                <img src="{{ asset('assets/img/jadwal.png') }}" alt="">
                <span>Jadwal</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link" aria-current="page">
                <img src="{{ asset('assets/img/pengumuman.png') }}" alt="">
                <span>Pengumuman</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link d-flex justify-content-between" data-bs-toggle="collapse"
                data-bs-target="#dashboard-collapse" aria-expanded="true" aria-current="page">
                <div><img src="{{ asset('assets/img/kelas.png') }}" alt="">
                    <span>Kelas</span>
                </div>
                <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
            <div class="collapse" id="dashboard-collapse" style="">
                <ul class="btn-toggle-nav list-unstyled list-dropdown-sidebar">
                    <li><a href="#" class="link-dark rounded">Kelas 7</a></li>
                    <li><a href="#" class="link-dark rounded">Kelas 8</a></li>
                    <li><a href="#" class="link-dark rounded">Kelas 9</a></li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link" aria-current="page">
                <img src="{{ asset('assets/img/quiz.png') }}" alt="">
                <span>Quiz</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link" aria-current="page">
                <img src="{{ asset('assets/img/ulangan.png') }}" alt="">
                <span>Ulangan</span>
            </a>
        </li>
        <li class="nav-item">
            <hr>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link" aria-current="page">
                <img src="{{ asset('assets/img/logout.png') }}" alt="">
                <span>Log Out</span>
            </a>
        </li>
    </ul>
</div>