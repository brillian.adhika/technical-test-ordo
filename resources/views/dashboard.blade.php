@extends('app')

@section('title', 'Dashboard')

@section('sidebar')
@include('parts/sidebar')
@endsection

@section('content')
<h1 class="page-title">Dashboard</h1>

<div class="row">
    <div class="col-md-4">
        <div class="top-detail-area p-5 d-flex justify-content-center">
            <div class="image-area d-flex flex-column align-items-center">
                <div class="image">
                    <img src="{{ asset('assets/img/avatar.png') }}" alt="">
                    <div class="online-status online"></div>
                </div>
                <div class="text align-center">
                    <h3>John Doe</h3>
                    <span><strong>Kelas 7-A</strong> | No. Absen 17</span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-8 daftar-pengumuman">
        <h2>Daftar pengumuman</h2>
        <div class="owl-carousel owl-theme">
            <div class="item">
                <div class="pengumuman">
                    <img src="{{ asset('assets/img/2661564 1.jpg') }}" alt="">
                    <h4>Judul Pengumuman</h4>
                    <span class="date">Senin 20 Januari, 2021</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ornare pretium placerat ut platea. Purus
                        blandit integer sagittis massa vel est hac. </p>

                    <button type="button" class="btn">READ MORE</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <h3 class="jadwal-title">Jadwal Pelajaran</h3>
    </div>
</div>

<div class="row px-2 mb-3">
    <div class="col-md-2 p-1 jadwal">
        <div class="head">
            <h5>Senin</h5>
        </div>
        <div class="body">
            <p class="title">Fisika</p>
            <p class="teacher">Jonathan Laura</p>
            <p class="time">08.00 - 10.00</p>
        </div>
    </div>

    <div class="col-md-2 p-1 jadwal">
        <div class="head">
            <h5>Selasa</h5>
        </div>
        <div class="body">
            <p class="title">Matematika</p>
            <p class="teacher">Jonathan Laura</p>
            <p class="time">08.00 - 10.00</p>
        </div>
    </div>

    <div class="col-md-2 p-1 jadwal">
        <div class="head">
            <h5>Rabu</h5>
        </div>
        <div class="body">
            <p class="title">Kimia</p>
            <p class="teacher">Jonathan Laura</p>
            <p class="time">08.00 - 10.00</p>
        </div>
    </div>

    <div class="col-md-2 p-1 jadwal">
        <div class="head">
            <h5>Kamis</h5>
        </div>
        <div class="body">
            <p class="title">Biologi</p>
            <p class="teacher">Jonathan Laura</p>
            <p class="time">08.00 - 10.00</p>
        </div>
    </div>

    <div class="col-md-2 p-1 jadwal">
        <div class="head">
            <h5>Jumat</h5>
        </div>
        <div class="body">
            <p class="title">Bahasa Inggris</p>
            <p class="teacher">Jonathan Laura</p>
            <p class="time">08.00 - 10.00</p>
        </div>
    </div>

    <div class="col-md-2 p-1 jadwal">
        <div class="head">
            <h5>Sabtu</h5>
        </div>
        <div class="body">
            <p class="title">Bahasa Indonesia</p>
            <p class="teacher">Jonathan Laura</p>
            <p class="time">08.00 - 10.00</p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <h3 class="jadwal-title">Jadwal Ulangan & Remidi</h3>
    </div>
</div>

<div class="row px-2 mb-3">
    <div class="col-md-2 p-1 jadwal">
        <div class="head">
            <h5>Senin</h5>
        </div>
        <div class="body d-flex justify-content-between align-items-center">
            <div>
                <p class="title">Fisika - 7A</p>
                <p class="teacher">Jonathan Laura</p>
                <p class="time">08.00 - 10.00</p>
            </div>
            <span class="badge bg-remidi">Remidi</span>
        </div>
    </div>

    <div class="col-md-2 p-1 jadwal">
        <div class="head">
            <h5>Selasa</h5>
        </div>
        <div class="body d-flex justify-content-between align-items-center">
            <div>
                <p class="title">Fisika - 7A</p>
                <p class="teacher">Jonathan Laura</p>
                <p class="time">08.00 - 10.00</p>
            </div>
            <span class="badge bg-ulangan">Ulangan</span>
        </div>
    </div>

    <div class="col-md-2 p-1 jadwal">
        <div class="head">
            <h5>Rabu</h5>
        </div>
        <div class="body d-flex justify-content-between align-items-center">
            <div>
                <p class="title">Fisika - 7A</p>
                <p class="teacher">Jonathan Laura</p>
                <p class="time">08.00 - 10.00</p>
            </div>
            <span class="badge bg-ulangan">Ulangan</span>
        </div>
    </div>

    <div class="col-md-2 p-1 jadwal">
        <div class="head">
            <h5>Kamis</h5>
        </div>
        <div class="body d-flex justify-content-between align-items-center">
            <div>
                <p class="title">Fisika - 7A</p>
                <p class="teacher">Jonathan Laura</p>
                <p class="time">08.00 - 10.00</p>
            </div>
            <span class="badge bg-ulangan">Ulangan</span>
        </div>
    </div>

    <div class="col-md-2 p-1 jadwal">
        <div class="head">
            <h5>Jumat</h5>
        </div>
        <div class="body d-flex justify-content-between align-items-center">
            <div>
                <p class="title">Fisika - 7A</p>
                <p class="teacher">Jonathan Laura</p>
                <p class="time">08.00 - 10.00</p>
            </div>
            <span class="badge bg-ulangan">Ulangan</span>
        </div>
    </div>

    <div class="col-md-2 p-1 jadwal">
        <div class="head">
            <h5>Sabtu</h5>
        </div>
        <div class="body d-flex justify-content-between align-items-center">
            <div>
                <p class="title">Fisika - 7A</p>
                <p class="teacher">Jonathan Laura</p>
                <p class="time">08.00 - 10.00</p>
            </div>
            <span class="badge bg-ulangan">Ulangan</span>
        </div>
    </div>
</div>
@endsection