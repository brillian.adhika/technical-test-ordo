@extends('app')

@section('title', 'Profile')

@section('sidebar')
@include('parts/sidebar')
@endsection

@section('content')
<h1 class="page-title">Profile</h1>

<div class="row">
    <div class="col-md-12">
        <div class="notes p-3 d-flex justify-content-between align-items-center mb-4">
            <div class="text">
                <h3>Notes!</h3>
                <p>Periksa data diri anda!<br />Jika terdapat ketidaksesuaian data segera hubungi admin untuk proses
                    perbaikan</p>
            </div>
            <button type="button" class="btn"><i class="fa fa-whatsapp" aria-hidden="true"></i> Hubungi Admin</button>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="top-detail-area">
            <div class="row">
                <div class="col-md-12 p-3 d-flex justify-content-center">
                    <div class="image-area d-flex flex-column align-items-center mb-4">
                        <div class="image">
                            <img src="{{ asset('assets/img/avatar.png') }}" alt="">
                            <div class="online-status online"></div>
                        </div>
                        <div class="text align-center">
                            <h3>John Doe</h3>
                            <span><strong>Kelas 7-A</strong> | No. Absen 17</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="bottom-detail-area py-3 px-5">
            <div class="row">
                <div class="col-md-4">
                    <h4>Email</h4>
                    <p>johndoe@email.com</p>
                </div>
                <div class="col-md-4">
                    <h4>NIP</h4>
                    <p>197404011996031002</p>
                </div>
                <div class="col-md-4">
                    <h4>Jenis Kelamin</h4>
                    <p>Laki-Laki</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <h4>Tanggal Lahir</h4>
                    <p>20 Juli 1996</p>
                </div>
                <div class="col-md-4">
                    <h4>Umur</h4>
                    <p>25 Tahun</p>
                </div>
                <div class="col-md-4">
                    <h4>Agama</h4>
                    <p>Islam</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <h4>Nama Orang Tua/Wali</h4>
                    <p>Simon Wijaya</p>
                </div>
                <div class="col-md-4">
                    <h4>Nomor Telepon orang tua</h4>
                    <p>+62 822-1234-5678</p>
                </div>
                <div class="col-md-4">
                    <h4>Tahun Masuk Sekolah</h4>
                    <p>2018</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4>Alamat</h4>
                    <p>Jln. Merak Merpati No. 456, Tanah Abang, Jakarta Utara, DKI Jakarta</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection