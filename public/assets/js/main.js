$(".owl-carousel").owlCarousel({
    loop: true,
    margin: 5,
    nav: true,
    responsive: {
        0: {
            items: 1.5,
        },

        1000: {
            items: 3.5,
        },
    },
});
